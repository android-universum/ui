/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.components.picker;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Calendar;

import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.fragment.annotation.ActionBarOptions;
import universum.studios.android.fragment.annotation.ContentView;
import universum.studios.android.samples.ui.R;
import universum.studios.android.samples.ui.ui.fragment.BaseSamplesFragment;
import universum.studios.android.ui.widget.CalendarView;

/**
 * @author Martin Albedinsky
 */
@ActionBarOptions(
		title = R.string.components_navigation_pickers_date_picker
)
@ContentView(R.layout.fragment_components_pickers_date)
public final class DatePickerFragment extends BaseSamplesFragment
		implements
		CalendarView.OnDateSelectionListener,
		CalendarView.OnMonthChangeListener,
		CalendarView.OnYearChangeListener {

	@SuppressWarnings("unused")
	private static final String TAG = "DatePickerFragment";

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		final Calendar calendarMax = Calendar.getInstance();
		final Calendar calendarMin = Calendar.getInstance();
		calendarMin.set(Calendar.YEAR, calendarMax.get(Calendar.YEAR) - 2);
		calendarMin.set(Calendar.MONTH, Calendar.DECEMBER);
		calendarMin.set(Calendar.DAY_OF_MONTH, 20);

		final CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendar_view);
		calendarView.setOnDateSelectionListener(this);
		calendarView.setOnMonthChangeListener(this);
		calendarView.setOnYearChangeListener(this);
		calendarView.getCalendarAdapter().setMinMaxDate(calendarMin.getTimeInMillis(), calendarMax.getTimeInMillis());
		calendarView.setVisibleDate(System.currentTimeMillis());
		view.findViewById(R.id.prev).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calendarView.smoothScrollToPreviousMonth();
			}
		});
		view.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calendarView.smoothScrollToNextMonth();
			}
		});
	}

	@Override
	public void onDateSelected(@NonNull CalendarView calendarView, long dateInMillis) {
		Log.d(TAG, "Selected date in calendar: " + dateInMillis);
	}

	@Override
	public void onNoDateSelected(@NonNull CalendarView calendarView) {
		Log.d(TAG, "No date selected in calendar.");
	}

	@Override
	public void onMonthScrolled(@NonNull CalendarView calendarView, @IntRange(from = Calendar.JANUARY, to = Calendar.DECEMBER) int month, @FloatRange(from = 0.0f, to = 1.0f) float offset) {
		Log.d(TAG, "Month scrolled to: " + offset);
	}

	@Override
	public void onMonthChanged(@NonNull CalendarView calendarView, @IntRange(from = Calendar.JANUARY, to = Calendar.DECEMBER) int month) {
		Log.d(TAG, "Calendar scrolled to month: " + month);
	}

	@Override
	public void onYearChanged(@NonNull CalendarView calendarView, int year) {
		Log.d(TAG, "Calendar scrolled to year: " + year);
	}
}