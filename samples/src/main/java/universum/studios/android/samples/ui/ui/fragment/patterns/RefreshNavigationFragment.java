/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.patterns;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.fragment.annotation.ActionBarOptions;
import universum.studios.android.samples.model.SampleItem;
import universum.studios.android.samples.ui.R;
import universum.studios.android.samples.ui.ui.adapter.SamplesAdapter;

/**
 * @author Martin Albedinsky
 */
@ActionBarOptions(title = R.string.patterns_navigation_swipe_to_refresh)
public final class RefreshNavigationFragment extends BasePatternsFragment {


	@Override
	protected void onBindSamples(@NonNull SamplesAdapter adapter) {
		final SampleItem.Builder builder = new SampleItem.Builder(getResources());
		final List<SampleItem> items = new ArrayList<>();
		items.add(createItem(
				builder,
				PatternsFragments.SWIPE_TO_REFRESH_LIST,
				R.string.patterns_navigation_swipe_to_refresh_list
		));
		items.add(createItem(
				builder,
				PatternsFragments.SWIPE_TO_REFRESH_GRID,
				R.string.patterns_navigation_swipe_to_refresh_grid
		));
		items.add(createItem(
				builder,
				PatternsFragments.SWIPE_TO_REFRESH_RECYCLER,
				R.string.patterns_navigation_swipe_to_refresh_recycler
		));
		items.add(createItem(
				builder,
				PatternsFragments.SWIPE_TO_REFRESH_WEB,
				R.string.patterns_navigation_swipe_to_refresh_web
		));
		adapter.changeItems(items);
	}
}