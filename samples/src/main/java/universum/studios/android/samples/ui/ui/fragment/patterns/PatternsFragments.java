/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.patterns;

import universum.studios.android.fragment.annotation.FactoryFragment;
import universum.studios.android.fragment.manage.BaseFragmentFactory;

/**
 * @author Martin Albedinsky
 */
public final class PatternsFragments extends BaseFragmentFactory {

	/*
	 * ERRORS ======================================================================================
	 */

	public static final int ERRORS_USER_INPUT = 0x20000001;

	public static final int ERRORS_APP = 0x20000002;

	public static final int ERRORS_INCOMPATIBLE_STATE = 0x20000003;

	/*
	 * NAVIGATION DRAWER ===========================================================================
	 */

	// No sub-sections.

	/*
	 * SWIPE TO REFRESH ============================================================================
	 */

	@FactoryFragment(RefreshListFragment.class)
	public static final int SWIPE_TO_REFRESH_LIST = 0xb0000001;

	@FactoryFragment(RefreshGridFragment.class)
	public static final int SWIPE_TO_REFRESH_GRID = 0xb0000002;

	@FactoryFragment(RefreshRecyclerFragment.class)
	public static final int SWIPE_TO_REFRESH_RECYCLER = 0xb0000003;

	@FactoryFragment(RefreshWebFragment.class)
	public static final int SWIPE_TO_REFRESH_WEB = 0xb0000004;

	public static final int SWIPE_TO_REFRESH_LAYOUT = 0xb0000005;

	/*
	 * SEARCH ======================================================================================
	 */

	// No sub-sections.
}