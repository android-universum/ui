/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.samples.ui.R;
import universum.studios.android.samples.ui.SamplesFragment;
import universum.studios.android.samples.ui.data.Extras;

/**
 * @author Martin Albedinsky
 */
public final class TextFragment extends SamplesFragment {

	@NonNull
	public static TextFragment newInstance(@Nullable CharSequence text) {
		final TextFragment fragment = new TextFragment();
		final Bundle args = new Bundle();
		args.putCharSequence(Extras.EXTRA_TEXT, text);
		fragment.setArguments(args);
		return fragment;
	}

	@NonNull
	public static TextFragment newInstance(@StringRes int resId) {
		final TextFragment fragment = new TextFragment();
		final Bundle args = new Bundle();
		args.putInt(Extras.EXTRA_TEXT_RES, resId);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_text, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.bindText(((TextView) view.findViewById(android.R.id.text1)));
	}

	private void bindText(TextView textView) {
		final Bundle args = getArguments();
		if (args.containsKey(Extras.EXTRA_TEXT)) {
			textView.setText(args.getCharSequence(Extras.EXTRA_TEXT));
		} else if (args.containsKey(Extras.EXTRA_TEXT_RES)) {
			textView.setText(args.getInt(Extras.EXTRA_TEXT_RES));
		}
	}
}