/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.data;

/**
 * @author Martin Albedinsky
 */
public final class Extras {

	private static final String PREFIX = "universum.studios.android.samples.ui.EXTRA.";

	public static final String EXTRA_TEXT = PREFIX + "Text";
	public static final String EXTRA_TEXT_RES = PREFIX + "TextRes";
	public static final String EXTRA_LAYOUT_RESOURCE = PREFIX + "LayoutResource";
	public static final String EXTRA_WINDOW_TRANSITION = PREFIX + "WindowTransition";
	public static final String EXTRA_TRANSITION_EXAMPLE = PREFIX + "TransitionExample";

	private Extras() {}
}