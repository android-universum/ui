/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.fragment.manage.FragmentController;
import universum.studios.android.samples.model.SampleItem;
import universum.studios.android.samples.ui.SamplesNavigationActivity;
import universum.studios.android.samples.ui.ui.MainActivity;
import universum.studios.android.samples.ui.ui.adapter.SamplesAdapter;

/**
 * @author Martin Albedinsky
 */
public abstract class BaseSamplesNavigationFragment extends BaseSamplesListFragment<SamplesAdapter> {

	protected FragmentController mFragmentController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final SamplesAdapter adapter = new SamplesAdapter(getActivity());
		onBindSamples(adapter);
		setAdapter(adapter);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.mFragmentController = ((MainActivity) getActivity()).getFragmentController();
	}

	@Override
	public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
		final SampleItem item = getAdapter().getItem(position);
		mFragmentController.newRequest((int) item.getId())
				.addToBackStack(true)
				.execute();
	}

	protected abstract void onBindSamples(@NonNull SamplesAdapter adapter);

	@NonNull
	protected SampleItem createItem(int id, @StringRes int titleResId) {
		return createItem(new SampleItem.Builder(getResources()), id, titleResId);
	}

	@NonNull
	protected SampleItem createItem(SampleItem.Builder builder, int id, @StringRes int titleResId) {
		return builder.reset().id(id).title(titleResId).build();
	}

	@Override
	public void onResume() {
		super.onResume();
		final Activity activity = getActivity();
		if (activity instanceof SamplesNavigationActivity) {
			((SamplesNavigationActivity) activity).setNavigationAccessible(true);
		}
	}
}