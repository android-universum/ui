/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.data;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import java.util.List;

import universum.studios.android.samples.ui.ui.adapter.AppsGridAdapter;

/**
 * @author Martin Albedinsky
 */
public final class AppsAsyncTask extends AsyncTask<Void, Void, List<ApplicationInfo>> {

	private final AppsGridAdapter mAppsAdapter;

	public AppsAsyncTask(AppsGridAdapter appsAdapter) {
		this.mAppsAdapter = appsAdapter;
	}

	@Override protected List<ApplicationInfo> doInBackground(Void... params) {
		if (mAppsAdapter != null) {
			final Context context = mAppsAdapter.getContext();
			final PackageManager packageManager = context.getPackageManager();
			if (packageManager != null) {
				return packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
			}

		}
		return null;
	}

	@Override protected void onPostExecute(List<ApplicationInfo> activities) {
		if (mAppsAdapter != null) mAppsAdapter.changeItems(activities);
	}
}