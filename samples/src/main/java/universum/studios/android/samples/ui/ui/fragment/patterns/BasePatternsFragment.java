/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.patterns;

import android.os.Bundle;

import androidx.annotation.Nullable;
import universum.studios.android.fragment.manage.FragmentFactory;
import universum.studios.android.samples.ui.ui.fragment.BaseSectionNavigationFragment;

/**
 * @author Martin Albedinsky
 */
public abstract class BasePatternsFragment extends BaseSectionNavigationFragment {

	private FragmentFactory mNavigationFragments;

	@Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Change fragment factory for the main fragment controller by the factory with fragments
		// specific for this section.
		this.mNavigationFragments = mFragmentController.getFactory();
		mFragmentController.setFactory(new PatternsFragments());
	}

	@Override public void onDestroyView() {
		super.onDestroyView();
		// Change back navigation fragments factory.
		mFragmentController.setFactory(mNavigationFragments);
	}
}