/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.components.picker;

import universum.studios.android.fragment.annotation.FactoryFragment;
import universum.studios.android.fragment.manage.BaseFragmentFactory;

/**
 * @author Martin Albedinsky
 */
final class PickersFragments extends BaseFragmentFactory {

	@FactoryFragment(NumberPickerFragment.class)
	static final int NUMBER_PICKER = 0x01;

	@FactoryFragment(DatePickerFragment.class)
	static final int DATE_PICKER = 0x02;
}