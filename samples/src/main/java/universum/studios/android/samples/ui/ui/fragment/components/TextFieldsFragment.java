/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.components;

import android.os.*;
import android.text.*;
import android.view.*;

import androidx.annotation.*;
import universum.studios.android.samples.ui.*;
import universum.studios.android.samples.ui.ui.fragment.*;
import universum.studios.android.ui.widget.*;

import static androidx.core.util.ObjectsCompat.*;

/**
 * @author Martin Albedinsky
 */
public class TextFieldsFragment
		extends BaseSamplesFragment
		implements EditLayout.OnInputLengthChangeListener {

	@SuppressWarnings("unused")
	private static final String TAG = "TextFieldsFragment";

	private EditLayout editLayoutError;

	public TextFieldsFragment() {
		super(R.layout.fragment_components_text_fields);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		view.findViewById(R.id.validate_input).setOnClickListener(this);

		editLayoutError = (EditLayout) view.findViewById(R.id.fragment_components_text_fields_error_field);

		final EditLayout editLayoutLengthConstraint1 = (EditLayout) view.findViewById(
				R.id.fragment_components_text_fields_length_constraint_field_1
		);
		final EditLayout editLayoutLengthConstraint2 = (EditLayout) view.findViewById(
				R.id.fragment_components_text_fields_length_constraint_field_2
		);

		editLayoutError.getInputView().addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// Ignored.
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// Ignored.
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s == null) {
					return;
				}

				if (s.toString().contentEquals("Error Text")) {
					editLayoutError.setError(s);
				} else {
					editLayoutError.clearError();
				}
			}
		});

		editLayoutLengthConstraint1.setOnInputLengthChangeListener(this);
		editLayoutLengthConstraint2.setOnInputLengthChangeListener(this);
	}

	@Override public void invalidateActionBar() {
		super.invalidateActionBar();

		requireNonNull(getSupportActionBar()).setTitle(R.string.components_navigation_text_fields);
	}

	@Override
	public void onInputLengthChanged(@NonNull EditLayout editLayout, int length) {
		int editLayoutId = editLayout.getId();

		if (editLayoutId == R.id.fragment_components_text_fields_length_constraint_field_1) {
			if (length > editLayout.getLengthConstraint()) {
				editLayout.setError("Maximum allowed count of characters exceeded.");
			} else {
				editLayout.clearError();
			}
		} else if (editLayoutId == R.id.fragment_components_text_fields_length_constraint_field_2) {
			if (editLayout.getText().toString().contains("error input")) {
				editLayout.setError("Error input entered");
			} else {
				editLayout.clearError();
			}
		}
	}

	@Override
	protected void onViewClick(@NonNull View view) {
		if (view.getId() == R.id.validate_input) {
			editLayoutError.clearError();
			editLayoutError.setError("Input is not valid");
		}
	}
}