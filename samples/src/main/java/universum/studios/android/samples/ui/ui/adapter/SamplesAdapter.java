/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import universum.studios.android.samples.model.SampleItem;
import universum.studios.android.samples.ui.R;
import universum.studios.android.widget.adapter.SimpleListAdapter;
import universum.studios.android.widget.adapter.holder.ViewHolder;

/**
 * @author Martin Albedinsky
 */
public final class SamplesAdapter extends SimpleListAdapter<SamplesAdapter, ViewHolder, SampleItem> {

	public SamplesAdapter(Context context) {
		super(context);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

	@NonNull
	@Override
	protected ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new ViewHolder(inflateView(R.layout.item_list_sample, parent));
	}

	@Override
	protected void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		((TextView) holder.itemView).setText(getItem(position).getTitle());
	}
}