/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui;

import android.os.*;
import android.view.*;

import androidx.annotation.*;
import androidx.fragment.app.*;
import universum.studios.android.fragment.*;
import universum.studios.android.fragment.annotation.*;
import universum.studios.android.fragment.manage.FragmentController;
import universum.studios.android.fragment.manage.*;
import universum.studios.android.fragment.transition.*;
import universum.studios.android.samples.ui.R;
import universum.studios.android.samples.ui.*;
import universum.studios.android.samples.ui.ui.fragment.*;
import universum.studios.android.samples.ui.ui.fragment.components.*;
import universum.studios.android.samples.ui.ui.fragment.patterns.*;
import universum.studios.android.samples.ui.ui.fragment.style.*;

/**
 * @author Martin Albedinsky
 */
public final class MainActivity extends SamplesNavigationActivity implements FragmentRequestInterceptor {

	static {
		FragmentAnnotations.setEnabled(true);
	}

	private FragmentController fragmentController;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fragmentController = FragmentController.create(this);
		fragmentController.setFactory(new NavigationFragments());
		fragmentController.setViewContainerId(R.id.samples_container);
	}

	@NonNull
	public FragmentController getFragmentController() {
		return fragmentController;
	}

	@Nullable
	@Override
	public Fragment interceptFragmentRequest(@NonNull FragmentRequest request) {
		request.transition(FragmentTransitions.CROSS_FADE).replaceSame(true);

		return null;
	}

	@Override
	protected boolean onHandleNavigationItemSelected(@NonNull MenuItem item) {
		int itemId = item.getItemId();

		if (itemId == R.id.navigation_item_home) {
			fragmentController.newRequest(new SamplesMainFragment()).execute();
		} else if (itemId == R.id.navigation_item_style) {
			fragmentController.newRequest(new StyleNavigationFragment()).execute();
		} else if (itemId == R.id.navigation_item_components) {
			fragmentController.newRequest(new ComponentsNavigationFragment()).execute();
		} else if (itemId == R.id.navigation_item_patterns) {
			fragmentController.newRequest(new PatternsNavigationFragment()).execute();
		}

		return true;
	}

	@Override
	public void onBackPressed() {
		if (dispatchBackPressToCurrentFragment()) {
			return;
		}

		final FragmentManager fragmentManager = getSupportFragmentManager();
		if (fragmentManager.getBackStackEntryCount() > 0 && fragmentManager.popBackStackImmediate()) {
			return;
		}

		super.onBackPressed();
	}

	private boolean dispatchBackPressToCurrentFragment() {
		final Fragment fragment = fragmentController.findCurrentFragment();

		return fragment instanceof BackPressWatcher watcherFragment && watcherFragment.dispatchBackPress();
	}
}