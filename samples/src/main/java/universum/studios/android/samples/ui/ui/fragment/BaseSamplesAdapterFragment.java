/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * @author Martin Albedinsky
 */
public abstract class BaseSamplesAdapterFragment<V extends ViewGroup, A> extends BaseSamplesFragment {

	private View emptyView;
	private A adapter;

	public void setAdapter(@Nullable A adapter) {
		if (this.adapter != null) {
			if (isViewCreated()) {
				onDetachAdapterFromView(this.adapter);
			}

			onDetachAdapter(this.adapter);
		}

		this.adapter = adapter;

		if (this.adapter != null) {
			onAttachAdapter(this.adapter);
		}

		if (isViewCreated() && this.adapter != null) {
			onAttachAdapterToView(this.adapter);
		}
	}

	public A getAdapter() {
		return adapter;
	}

	protected void onAttachAdapter(@NonNull A adapter) {
	}

	protected void onDetachAdapter(@NonNull A adapter) {
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		emptyView = view.findViewById(android.R.id.empty);

		if (adapter != null) {
			onAttachAdapterToView(adapter);
		}
	}

	@SuppressWarnings({"ConstantConditions", "unchecked"})
	protected V findAdapterView() {
		return isViewCreated() ? (V) getView().findViewById(android.R.id.list) : null;
	}

	@Nullable
	protected View getEmptyView() {
		return emptyView;
	}

	protected abstract void onAttachAdapterToView(@NonNull A adapter);

	protected abstract void onDetachAdapterFromView(@NonNull A adapter);

	public void setEmptyText(@StringRes int resId) {
		setEmptyText(getResources().getText(resId));
	}

	public void setEmptyText(@Nullable CharSequence text) {
		if (emptyView instanceof TextView) {
			((TextView) emptyView).setText(text);
		}
	}
}