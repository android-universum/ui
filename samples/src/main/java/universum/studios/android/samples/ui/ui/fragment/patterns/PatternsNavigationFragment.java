/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.patterns;

import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import universum.studios.android.fragment.annotation.ActionBarOptions;
import universum.studios.android.samples.model.SampleItem;
import universum.studios.android.samples.ui.R;
import universum.studios.android.samples.ui.ui.SearchActivity;
import universum.studios.android.samples.ui.ui.adapter.SamplesAdapter;
import universum.studios.android.samples.ui.ui.fragment.BaseSamplesNavigationFragment;
import universum.studios.android.samples.ui.ui.fragment.NavigationFragments;

/**
 * @author Martin Albedinsky
 */
@ActionBarOptions(title = R.string.navigation_item_patterns)
public final class PatternsNavigationFragment extends BaseSamplesNavigationFragment {

	@Override
	protected void onBindSamples(@NonNull SamplesAdapter adapter) {
		final List<SampleItem> items = new ArrayList<>();
		items.add(createItem(
				NavigationFragments.PATTERNS_SEARCH,
				R.string.patterns_navigation_search
		));
		items.add(createItem(
				NavigationFragments.PATTERNS_SWIPE_TO_REFRESH,
				R.string.patterns_navigation_swipe_to_refresh
		));
		adapter.changeItems(items);
	}

	@Override
	public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
		switch ((int) id) {
			case NavigationFragments.PATTERNS_SEARCH:
				SearchActivity.launch(getActivity());
				break;
			default:
				super.onItemClick(parent, view, position, id);
		}
	}
}