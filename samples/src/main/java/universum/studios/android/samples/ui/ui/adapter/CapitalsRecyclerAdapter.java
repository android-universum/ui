/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.adapter;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author Martin Albedinsky
 */
public final class CapitalsRecyclerAdapter extends RecyclerView.Adapter<CapitalsRecyclerAdapter.ItemHolder> {

	private final CapitalsListAdapter mListAdapter;

	public CapitalsRecyclerAdapter(Context context) {
		this.mListAdapter = new CapitalsListAdapter(context);
	}

	@Override
	public int getItemCount() {
		return mListAdapter.getItemCount();
	}

	@Override
	public ItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
		return new ItemHolder(mListAdapter.onCreateViewHolder(viewGroup, position));
	}

	@Override
	public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
		mListAdapter.onBindViewHolder(holder.holder, position);
	}

	static final class ItemHolder extends RecyclerView.ViewHolder {

		final CapitalsListAdapter.ItemHolder holder;

		ItemHolder(@NonNull CapitalsListAdapter.ItemHolder holder) {
			super(holder.itemView);
			this.holder = holder;
		}
	}
}