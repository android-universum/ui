/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.*;
import universum.studios.android.fragment.ActionBarFragment;
import universum.studios.android.samples.ui.SamplesNavigationActivity;

/**
 * @author Martin Albedinsky
 */
public abstract class BaseSamplesFragment extends ActionBarFragment implements View.OnClickListener {

	protected BaseSamplesFragment() {
		this(0);
	}

	protected BaseSamplesFragment(@LayoutRes final int contentLayoutId) {
		super(contentLayoutId);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final Activity activity = getActivity();

		if (activity instanceof SamplesNavigationActivity) {
			((SamplesNavigationActivity) activity).setNavigationAccessible(false);
		}
	}

	@Override
	public void onClick(View view) {
		onViewClick(view);
	}
}