/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.components.collection;

import universum.studios.android.fragment.annotation.FactoryFragment;
import universum.studios.android.fragment.manage.BaseFragmentFactory;

/**
 * @author Martin Albedinsky
 */
final class CollectionFragments extends BaseFragmentFactory {

	/*
	 * GRIDS =======================================================================================
	 */

	@FactoryFragment(SimpleGridFragment.class)
	static final int GRID_SIMPLE = 0x10000000;

	/*
	 * LISTS =======================================================================================
	 */

	@FactoryFragment(SimpleListFragment.class)
	static final int LIST_SIMPLE = 0x20000000;

	/*
	 * PAGERS ======================================================================================
	 */

	static final int PAGER = 0x30000000;
}