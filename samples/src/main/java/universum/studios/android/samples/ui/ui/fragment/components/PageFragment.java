/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment.components;

import android.annotation.*;
import android.os.*;
import android.view.*;
import android.widget.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import universum.studios.android.samples.ui.*;

/**
 * @author Martin Albedinsky
 */
public final class PageFragment extends SamplesFragment {

	private static final String PAGE_LABEL_FORMAT = "%d PAGE";
	private static final String PARAM_INDEX = "universum.studios.android.samples.ui.fragment.components.PageFragment.PARAM.Index";

	private int index;

	public static PageFragment newInstance(int index) {
		final PageFragment fragment = new PageFragment();

		final Bundle params = new Bundle();

		params.putInt(PARAM_INDEX, index);

		fragment.setArguments(params);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		index = requireArguments().getInt(PARAM_INDEX);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_page, container, false);
	}

	@Override
	@SuppressLint("DefaultLocale")
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		((TextView) view.findViewById(R.id.fragment_page_text_view_label)).setText(
			String.format(PAGE_LABEL_FORMAT, index + 1)
		);
	}
}