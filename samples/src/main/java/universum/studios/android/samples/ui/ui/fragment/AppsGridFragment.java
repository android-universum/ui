/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.ui.ui.fragment;

import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import universum.studios.android.samples.ui.R;
import universum.studios.android.samples.ui.data.AppsAsyncTask;
import universum.studios.android.samples.ui.ui.adapter.AppsGridAdapter;

/**
 * @author Martin Albedinsky
 */
public class AppsGridFragment extends BaseSamplesGridFragment<AppsGridAdapter> {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final AppsGridAdapter adapter = new AppsGridAdapter(getActivity());
		setAdapter(adapter);
		new AppsAsyncTask(adapter).execute();
	}

	@Override
	public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setEmptyText(R.string.components_collection_empty);
	}

	@Override
	@SuppressWarnings("ConstantConditions")
	public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
		final ApplicationInfo appInfo = getAdapter().getItem(position);
		if (appInfo != null) {
			try {
				startActivity(getAdapter().getPackageManager().getLaunchIntentForPackage(appInfo.packageName));
			} catch (Exception e) {
				Toast.makeText(getActivity(), "No permission to launch " + appInfo.packageName, Toast.LENGTH_SHORT).show();
			}
		}
	}
}