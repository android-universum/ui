Ui-Graphics-Core
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aui/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aui/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:ui-graphics-core:${DESIRED_VERSION}@aar"

_depends on:_
[ui-core](https://bitbucket.org/android-universum/ui/src/main/library-core)