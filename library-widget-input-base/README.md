Ui-Widget-Input-Base
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aui/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aui/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:ui-widget-input-base:${DESIRED_VERSION}@aar"

_depends on:_
[ui-widget-text](https://bitbucket.org/android-universum/ui/src/main/library-widget-text),
[ui-widget-layout-common](https://bitbucket.org/android-universum/ui/src/main/library-widget-layout-common)