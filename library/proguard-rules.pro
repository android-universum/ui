# Keep names of all classes and their methods form the UI package.
-keepnames class universum.studios.android.ui.** { *; }
# Keep constructors for all UI widgets.
-keepclasseswithmembers class universum.studios.android.ui.widget.** {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public <init>(android.content.Context, android.util.AttributeSet, int, int);
}