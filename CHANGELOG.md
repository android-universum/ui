Change-Log
===============
> Regular maintenance: _01.03.2024_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 0.x ##

### 0.10.11 ###
> 27.02.2024

- Regular **maintenance**.

### 0.10.10 ###
> 27.08.2020

- Fix [Issue #14](https://bitbucket.org/android-universum/ui/issues/14).

### 0.10.9 ###
> 05.08.2020

- Fix [Issue #13](https://bitbucket.org/android-universum/ui/issues/13).

### 0.10.8 ###
> 31.05.2020

- Use widgets from Android Jetpack also in layout files.

### 0.10.7 ###
> 09.05.2020

- Regular **maintenance**.

### 0.10.6 ###
> 13.01.2020

- Resolved [Issue #11](https://bitbucket.org/android-universum/ui/issues/11).

### 0.10.5 ###
> 07.01.2020

- Resolved [Issue #9](https://bitbucket.org/android-universum/ui/issues/9).
- Resolved [Issue #10](https://bitbucket.org/android-universum/ui/issues/10).

### 0.10.4 ###
> 08.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### 0.10.3 ###
> 24.05.2019

- Stability improvements.

### 0.10.2 ###
> 24.01.2019

- Small updates and improvements.
- Resolved [Issue #4](https://bitbucket.org/android-universum/ui/issues/4).
- Resolved [Issue #5](https://bitbucket.org/android-universum/ui/issues/5).

### 0.10.1 ###
> 09.12.2018

- Allowed to set a desired **TimeZone** for `CalendarView`.

### 0.10.0 ###
> 19.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### 0.9.6 ###
> 22.10.2017

- **Resolved** some of reported issues.

### 0.9.5 ###
> 01.08.2017

- **Dropped support** for _Android_ versions **below** _API Level 14_.

### 0.9.4 ###
> 20.07.2017

- Stability improvements.

### 0.9.3 ###
> 14.06.2017

- Removed `<item name="android:numColumns">auto_fit</item>` from styles for **grid collections** as
  it was causing new **AAPT 2** tool to fail when building project.

### 0.9.2 ###
> 15.05.2017

- Stability improvements.

### 0.9.1 ###
> 02.03.2017

- Pre-release patches.

### 0.9.0 ###
> 23.01.2017

- First pre-release.
