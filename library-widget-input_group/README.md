@Ui-Widget-Input
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aui/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aui/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:ui-widget-input:${DESIRED_VERSION}@aar"

_depends on:_
[ui-widget-edit](https://bitbucket.org/android-universum/ui/src/main/library-widget-edit),
[ui-widget-menu](https://bitbucket.org/android-universum/ui/src/main/library-widget-menu)