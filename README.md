Android UI
===============

[![CircleCI](https://circleci.com/bb/android-universum/ui.svg?style=shield)](https://circleci.com/bb/android-universum/ui)
[![Codecov](https://codecov.io/bb/android-universum/ui/graph/badge.svg?token=T41m5AILdh)](https://codecov.io/bb/android-universum/ui)
[![Codacy](https://app.codacy.com/project/badge/Grade/196be9ea377b48a1b84c69942fff8d52)](https://app.codacy.com/bb/android-universum/ui/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Android](https://img.shields.io/badge/android-14.0-blue.svg)](https://developer.android.com/about/versions/14)
[![Robolectric](https://img.shields.io/badge/robolectric-4.11.1-blue.svg)](http://robolectric.org)
[![Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

Extended UI elements, views and widgets for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/ui/wiki)**.

## Under Development ##

**Note that this project is under outgoing development and its implementation may change
over time and thus it is NOT RECOMMENDED to USE IT IN a PRODUCTION environment.**

## Download ##
[![Maven](https://img.shields.io/maven-central/v/io.bitbucket.android-universum/ui)](https://search.maven.org/artifact/io.bitbucket.android-universum/ui)

Download the latest **[artifacts](https://bitbucket.org/android-universum/ui/downloads "Downloads page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "io.bitbucket.android-universum:ui:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/ui/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.legacy:legacy-support-v4`](https://developer.android.com/jetpack/androidx)
- [`androidx.appcompat:appcompat`](https://developer.android.com/jetpack/androidx)
- [`androidx.vectordrawable:vectordrawable`](https://developer.android.com/jetpack/androidx)
- [`androidx.recyclerview:recyclerview`](https://developer.android.com/jetpack/androidx)
- [`universum.studios.android:graphics-color-util`](https://bitbucket.org/android-universum/graphics/src/main/MODULES.md)
- [`universum.studios.android:font-core`](https://bitbucket.org/android-universum/font/src/main/MODULES.md)

## [License](https://bitbucket.org/android-universum/ui/src/main/LICENSE.md) ##

**Copyright 2024 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.