Ui-Widget-Input-Menu
===============

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aui/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aui/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:ui-widget-input-menu:${DESIRED_VERSION}@aar"

_depends on:_
[ui-widget-menu](https://bitbucket.org/android-universum/ui/src/main/library-widget-menu),
[ui-widget-input-base](https://bitbucket.org/android-universum/ui/src/main/library-widget-input-base)